package com.chronoboss.chronoboss.ui.notifications

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.util.Log
import java.util.*

/** get list of all installed apps, their names and icons, filtered to exclude system apps
 * @return filteredPackages
 */
fun getInstalledApps(ctx: Context): MutableSet<PackageInfo?> {
    val packageManager = ctx.packageManager
    val allInstalledPackages = packageManager.getInstalledPackages(PackageManager.GET_META_DATA)
    val filteredPackages: MutableSet<PackageInfo?> = HashSet<PackageInfo?>()
    val defaultActivityIcon = packageManager.defaultActivityIcon
    for (each in allInstalledPackages) {
        if (ctx.packageName == each.packageName) {
            continue
        }
        try {
            val intentOfStartActivity =
                packageManager.getLaunchIntentForPackage(each.packageName)
                    ?: continue
            val applicationIcon = packageManager.getActivityIcon(intentOfStartActivity)
            if (applicationIcon != null && defaultActivityIcon != applicationIcon) {
                filteredPackages.add(each)
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.i("MyTag", "Unknown package name " + each.packageName)
        }
    }
    return filteredPackages
}



