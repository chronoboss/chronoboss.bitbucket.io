package com.chronoboss.chronoboss.ui.home

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chronoboss.chronoboss.R
import com.chronoboss.chronoboss.databinding.FragmentHomeBinding


/**
 * Home fragment.
 *
 */
class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    val bohemianRhapsody: String = "You've used productive apps for the length of the iconic Queen song!"
    val martianSignals:String = "You've earned enough time to send a signal from Earth to Mars!"
    val goodSleep:String = "You've used productive apps for 8 hours, hopefully not when you're meant to be sleeping!"
    val lordOfRings:String = "Oh no! You've wasted enough time to watch the entire Lord of the Rings trilogy!"
    val controlFreak:String = "You've chosen Settings as an app to track, you weirdo!"
    val changeHeart:String = "Naw, you clicked on our About page! <3"
    val shutUp:String = "Wow ok, you turned off our voice alerts? Hurtful! </3"
    val qaTester:String = "We should hire you to test our app! You've spent over an hour on Chronoboss!"
    val medalMedal:String = "Congratulations! You've won all of our medals now, you star!"

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    /** Initialises view.
     * @return root
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //val boh: ImageButton = binding.medalBohemian
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textHome
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        var bohemian:Boolean? = sharedPref?.getBoolean("bohemianPreference", false)
        var martian:Boolean? = sharedPref?.getBoolean("martianPreference", false)
        var sleepy:Boolean? = sharedPref?.getBoolean("sleepyPreference", false)
        var ring:Boolean? = sharedPref?.getBoolean("ringPreference", false)
        var settingsMedal:Boolean? = sharedPref?.getBoolean("settingsPreference", false)
        var qa:Boolean? = sharedPref?.getBoolean("qaPreference", false)
        var shutup:Boolean? = sharedPref?.getBoolean("shutupPreference", false)
        var medmed:Boolean? = sharedPref?.getBoolean("medalPreference", false)
        var heart:Boolean? = sharedPref?.getBoolean("heartPreference", false)



        var pckN: String? = sharedPref?.getString("pckg1", "No value")
        var pckN2: String? = sharedPref?.getString("pckg2", "No value")
        var pckN3: String? = sharedPref?.getString("pckg3", "No value")
        var pckN4: String? = sharedPref?.getString("pckg4", "No value")
        var prodNme1:String? = getAppNameFromPackage(context, pckN)
        var prodNme2: String? = getAppNameFromPackage(context, pckN2)
        var unprodNme1:String? = getAppNameFromPackage(context, pckN3)
        var unprodNme2: String? = getAppNameFromPackage(context, pckN4)

        var endTime: Long = System.currentTimeMillis()

        var startTime: Long = (System.currentTimeMillis() - 86400000)

        var pck1 = getPckUsg(context, pckN, startTime, endTime)
        var Long1 =
            pck1?.totalTimeInForeground //Toast.makeText(context, Long1.toString(), Toast.LENGTH_LONG).show()
        var int1:Int? = (Long1?.div(60000))?.toInt()
        var pck2 = getPckUsg(context, pckN2, startTime, endTime)
        var Long2 = pck2?.totalTimeInForeground
        var int2:Int? = (Long2?.div(60000))?.toInt()
        var pck3 = getPckUsg(context, pckN3, startTime, endTime)
        var Long3 = pck3?.totalTimeInForeground
        var int3:Int? = (Long3?.div(60000))?.toInt()
        var pck4 = getPckUsg(context, pckN4, startTime, endTime)
        var Long4 = pck4?.totalTimeInForeground
        var int4:Int? = (Long4?.div(60000))?.toInt()

        var chronUsg = getPckUsg(context, "com.chronoboss.chronoboss", startTime, endTime)
        var chronLong = chronUsg?.totalTimeInForeground
        var chronInt = (chronLong?.div(60000))?.toInt()



        val medBohemian: ImageButton = binding.medalBohemian
        val medMars:ImageButton = binding.medalMars
        val medRest:ImageButton = binding.medalRest
        val medRing:ImageButton = binding.medalRing
        val medControl:ImageButton = binding.medalControl
        val medHeart:ImageButton = binding.medalHeart
        val medShutup:ImageButton = binding.medalShutup
        val medQaTester:ImageButton = binding.medalQa
        val medMed:ImageButton = binding.medalMedal
        var medTxt:TextView = binding.medalText
        var medTitle:TextView = binding.medalTitle

        var totalProductive:Int = 0
        var totalUnproductive:Int = 0


        if(int1 != null){
            totalProductive += int1
        }
        if(int2 != null){
            totalProductive += int2
        }

        if((int3 != null) || int4 != null){
            if(int3 != null){
                totalUnproductive += int3
            }
            if(int4 != null){
                totalUnproductive += int4
            }
        }


        if((sharedPref != null) && (bohemian == false) && (totalProductive >= 6)){
            with(sharedPref.edit()) {
                putBoolean("bohemianPreference", true).apply()
            }
        }

        if (chronInt != null) {
            if((sharedPref != null) && (qa == false) && (chronInt >= 60)){
                with(sharedPref.edit()) {
                    putBoolean("qaPreference", true).apply()
                }
            }
        }

        if((sharedPref != null) && (martian == false) && (totalProductive >= 21)){
            with(sharedPref.edit()) {
                putBoolean("martianPreference", true).apply()
            }
        }

        if((sharedPref != null) && (sleepy == false) && (totalProductive >= 11520)){
            with(sharedPref.edit()) {
                putBoolean("sleepyPreference", true).apply()
            }
        }

        if((sharedPref != null) && (ring == false) && (totalUnproductive >= 12978)){
            with(sharedPref.edit()) {
                putBoolean("ringPreference", true).apply()
            }
        }

        if((bohemian != null) && (bohemian == true)){
            medBohemian.setImageResource(R.drawable.medalbohemian)
        }

        if((martian != null) && (martian == true)){
            medMars.setImageResource(R.drawable.medalmars)
        }

        if((heart != null) && (heart == true)){
            medHeart.setImageResource(R.drawable.medalheart)
        }

        if((sleepy != null) && (sleepy == true)){
            medRest.setImageResource(R.drawable.medalrest)
        }

        if((ring != null) && (ring == true)){
            medRest.setImageResource(R.drawable.medalring)
        }

        if((qa != null) && (qa == true)){
            medQaTester.setImageResource(R.drawable.medalqa)
        }

        if((shutup != null) && (shutup == true)){
            medShutup.setImageResource(R.drawable.medalshutup)
        }

        var settingsMed:Boolean? = sharedPref?.getBoolean("settingsPreference", false)
        if((settingsMed != null) && (settingsMed == true)){
            medControl.setImageResource(R.drawable.medalcontrol)
        }

        if((bohemian != null) &&
            (bohemian == true) && (martian != null) && (martian == true) &&
            (sleepy != null) && (sleepy == true) && (ring != null) && (ring == true) &&
            (qa != null) && (qa == true) && (settingsMed != null) && (settingsMed == true) &&
            (shutup != null) && (shutup == true) && (heart != null) && (heart == true)) {
            with(sharedPref.edit()) {
                putBoolean("medalPreference", true).apply()
            }
        }

        if((medmed != null) && (medmed == true)){
            medMed.setImageResource(R.drawable.medalmedal)
        }





        medBohemian.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Bohemian Rhapsody"
                medTxt.text = bohemianRhapsody
            }
        })

        medMars.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Martian Signals"
                medTxt.text = martianSignals

                // Add your Staff
            }
        })

        medRest.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Good Night's Rest"
                medTxt.text = goodSleep

                // Add your Staff
            }
        })

        medRing.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Lord of the Rings"
                medTxt.text = lordOfRings

                // Add your Staff
            }
        })

        medControl.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Control Freak"
                medTxt.text = controlFreak

                // Add your Staff
            }
        })

        medHeart.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Warm Our Hearts"
                medTxt.text = changeHeart

                // Add your Staff
            }
        })

        medShutup.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Shut Up"
                medTxt.text = shutUp

                // Add your Staff
            }
        })

        medQaTester.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "QA Tester"
                medTxt.text = qaTester

                // Add your Staff
            }
        })

        medMed.setOnClickListener(object : View.OnClickListener {
            override fun onClick(arg0: View?) {
                medTitle.text = "Medal Medal"
                medTxt.text = medalMedal

                // Add your Staff
            }
        })

        //val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)




        return root
    }

    fun getPckUsg(context: Context?, str: String?, beginTime: Long, endTime: Long): UsageStats? {
        val usageStatsManager =
            context?.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
        var usages: Map<String, UsageStats> =
            usageStatsManager.queryAndAggregateUsageStats(beginTime, endTime)
        return usages[str]
    }

    private fun getAppNameFromPackage(context: Context?, packageName: String?): String? {
        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pkgAppsList = context?.packageManager
            ?.queryIntentActivities(mainIntent, 0)
        if (pkgAppsList != null) {
            for (app in pkgAppsList) {
                if (app.activityInfo.packageName == packageName) {
                    return app.activityInfo.loadLabel(context?.packageManager).toString()
                }
            }
        }
        return null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}