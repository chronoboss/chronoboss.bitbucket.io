package com.chronoboss.chronoboss.ui.dashboard

import android.app.AppOpsManager
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chronoboss.chronoboss.R
import com.chronoboss.chronoboss.databinding.FragmentDashboardBinding
import org.w3c.dom.Text


/**
 * Dashboard fragment.
 *
 */
class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    /** View initialised when screen opened.
     * @return view
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val iconProd1: ImageView? = binding.productive1img
        val iconProd2: ImageView? = binding.productive2img
        val iconUnprod1: ImageView? = binding.unproductive1img
        val iconUnprod2: ImageView? = binding.unrproductive2img
        val txtProd1:TextView? = binding.productive1
        val txtProd2:TextView? = binding.productive2
        val txtUnprod1:TextView? = binding.unproductive1
        val txtUnprod2:TextView? = binding.unproductive2
        if (!hasUsageStatsPermission()) {

            requestUsageStatsPermission()

        }

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        var pckN: String? = sharedPref?.getString(getString(R.string.p1), getString(R.string.defaultvalue))
        var pckN2: String? = sharedPref?.getString(getString(R.string.p2), getString(R.string.defaultvalue))
        var pckN3: String? = sharedPref?.getString(getString(R.string.p3), getString(R.string.defaultvalue))
        var pckN4: String? = sharedPref?.getString(getString(R.string.p4), getString(R.string.defaultvalue))
        var prodNme1:String? = getAppNameFromPackage(context, pckN)
        var prodNme2: String? = getAppNameFromPackage(context, pckN2)
        var unprodNme1:String? = getAppNameFromPackage(context, pckN3)
        var unprodNme2: String? = getAppNameFromPackage(context, pckN4)
        var endTime: Long = System.currentTimeMillis()

        var startTime: Long = (System.currentTimeMillis() - 86400000)

        var pck1 = getPckUsg(context, pckN, startTime, endTime)
        var Long1 =
            pck1?.totalTimeInForeground //Toast.makeText(context, Long1.toString(), Toast.LENGTH_LONG).show()
        var int1:Int? = (Long1?.div(60000))?.toInt()
        var pck2 = getPckUsg(context, pckN2, startTime, endTime)
        var Long2 = pck2?.totalTimeInForeground
        var int2:Int? = (Long2?.div(60000))?.toInt()
        var pck3 = getPckUsg(context, pckN3, startTime, endTime)
        var Long3 = pck3?.totalTimeInForeground
        var int3:Int? = (Long3?.div(60000))?.toInt()
        var pck4 = getPckUsg(context, pckN4, startTime, endTime)
        var Long4 = pck4?.totalTimeInForeground
        var int4:Int? = (Long4?.div(60000))?.toInt()
        var totalBalanceLong: Long = 0
        var icon:Drawable= getResources().getDrawable(R.drawable.ic_question_circle_solid_1_)
        try {
            if (pckN != getString(R.string.defaultvalue)) {
                var pckIcon: Drawable? =
                    pckN?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon != null) {
                    iconProd1?.setImageDrawable(pckIcon)
                } else {
                    iconProd1?.setImageDrawable(icon)
                }
                txtProd1?.setText(prodNme1)
            }
            if (pckN2 != getString(R.string.defaultvalue)) {
                var pckIcon2: Drawable? =
                    pckN2?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon2 != null) {
                    iconProd2?.setImageDrawable(pckIcon2)
                } else {
                    iconProd2?.setImageDrawable(icon)
                }
                txtProd2?.setText(prodNme2)
            }
            if (pckN3 != getString(R.string.defaultvalue)) {
                var pckIcon3: Drawable? =
                    pckN3?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon3 != null) {
                    iconUnprod1?.setImageDrawable(pckIcon3)
                } else {

                    iconUnprod1?.setImageDrawable(icon)
                }
                txtUnprod1?.setText(unprodNme1)
            }

            if (pckN4 != "No value") {
                var pckIcon4: Drawable? =
                    pckN4?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon4 != null) {
                    iconUnprod2?.setImageDrawable(pckIcon4)
                } else {
                    iconUnprod2?.setImageDrawable(icon)
                }
                txtUnprod2?.setText(unprodNme2)
            }
        }
        catch (e: java.lang.NullPointerException){ // Catch for e: Exception if errors are prone. Not good practice, I know.
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            with(sharedPref.edit()){
                remove("pckg1")
                remove("pckg2")
                remove("pckg3")
                remove("pckg4")
                apply()
            }
        }
        if (Long1 != null) {
            totalBalanceLong += Long1
        }
        if(Long2 != null){
            totalBalanceLong += Long2
        }

        if(Long3 != null){
            totalBalanceLong -= Long3
        }


        if(Long4 != null){
            totalBalanceLong -= Long4
        }


        var totalBalanceInt: Int = (totalBalanceLong/60000).toInt()
        var txtBalanceView: TextView? = binding.textView16
        txtBalanceView?.setText(totalBalanceInt.toString())
        var txtProdChange1:TextView = binding.productive1change
        var txtProd1Num:String? = ("+" + int1.toString())
        txtProdChange1.setText(if (txtProd1Num == "+null") "+0" else txtProd1Num)
        var txtProdChange2:TextView = binding.productive2change
        var txtProd2Num:String? = ("+" + int2.toString())
        txtProdChange2.setText(if (txtProd2Num == "+null") "+0" else txtProd2Num)
        var txtunprodChange1:TextView = binding.unproductive1change
        var txtUnprod1Num:String? = ("-" + int3.toString())
        txtunprodChange1.setText(if (txtUnprod1Num == "-null") "-0" else txtUnprod1Num)
        var txtunprodChange2:TextView = binding.unproductive2change
        var txtUnprod2Num:String? = ("-" + int4.toString())
        txtunprodChange2.setText(if (txtUnprod2Num == "-null") "-0" else txtUnprod2Num)

        val textView: TextView = binding.textDashboard
        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /** Function to query usage stats and return the target package based on a
     * package name.
     * @return usages[str] the target package
     */
    fun getPckUsg(context: Context?, str: String?, beginTime: Long, endTime: Long): UsageStats? {
        val usageStatsManager =
            context?.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
        var usages: Map<String, UsageStats> =
            usageStatsManager.queryAndAggregateUsageStats(beginTime, endTime)
        return usages[str]
    }

    /** Function to check whether the user has granted Chronoboss
     * the ability to access usasge stats from the OS.
     * @return (mode == AppOpsManager.MODE_ALLOWED) true if usage access granted
     */
    fun hasUsageStatsPermission(): Boolean {

        val applicationInform: ApplicationInfo? =

            context?.packageName?.let { activity?.packageManager?.getApplicationInfo(it, 0) }

        val appOps: AppOpsManager =

            context?.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager

        val mode: Int? = applicationInform?.let {

            appOps.checkOpNoThrow(

                AppOpsManager.OPSTR_GET_USAGE_STATS,

                it.uid,

                applicationInform.packageName

            )

        }

        return (mode == AppOpsManager.MODE_ALLOWED)

    }


    /** Function to request access from the user for usage stats from the OS
     *
     */
    fun requestUsageStatsPermission() {

        startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))

    }

    /** Function to get the application name of a package, based on its
     * package name.
     * @return the application name or null
     */
    private fun getAppNameFromPackage(context: Context?, packageName: String?): String? {
        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pkgAppsList = context?.packageManager
            ?.queryIntentActivities(mainIntent, 0)
        if (pkgAppsList != null) {
            for (app in pkgAppsList) {
                if (app.activityInfo.packageName == packageName) {
                    return app.activityInfo.loadLabel(context?.packageManager).toString()
                }
            }
        }
        return null
    }
}