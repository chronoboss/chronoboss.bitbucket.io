package com.chronoboss.chronoboss.ui.notifications

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chronoboss.chronoboss.*
import com.chronoboss.chronoboss.databinding.FragmentNotificationsBinding
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private var _binding: FragmentNotificationsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProvider(this).get(NotificationsViewModel::class.java)

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root


        val textView: TextView = binding.textNotifications
        notificationsViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        loadData()

        return binding.root
    }

    /**
     * Loads data from shared preferences.
     * Needs to iterate over UI elements or store said elements in a list and iterate over them.
     * I'm justifying doing this manually due to the smaller number of settings required.
     */
    private fun loadData() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPref?.getBoolean("switch2", false)?.let { binding.switch2.setChecked(it) }
        sharedPref?.getBoolean("switchHorror", false)?.let { binding.switchHorror.setChecked(it) }

        val icon: ImageView? = binding.addProdApp
        val button: Button? = binding.button
        val icon2: ImageView? = binding.button3
        val icon3: ImageView? = binding.addProdApp3
        val icon4: ImageView? = binding.button5
        var pckN: String? = sharedPref?.getString("pckg1", "No value")
        var pckN2: String? = sharedPref?.getString("pckg2", "No value")
        var pckN3: String? = sharedPref?.getString("pckg3", "No value")
        var pckN4: String? = sharedPref?.getString("pckg4", "No value")
        //var settingsMedal:Boolean? = sharedPref?.getBoolean("settingsPreference", false)
        try {
            if (pckN != "No value") {
                var pckIcon: Drawable? =
                    pckN?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon != null) {
                    icon?.setImageDrawable(pckIcon)
                }
            } else{
                icon?.setImageResource(R.drawable.ic_plus_circle_solid)
            }
            if (pckN2 != "No value") {
                var pckIcon2: Drawable? =
                    pckN2?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon2 != null) {
                    icon2?.setImageDrawable(pckIcon2)
                }
            } else {
                icon2?.setImageResource(R.drawable.ic_plus_circle_solid)
            }
            if (pckN3 != "No value") {
                var pckIcon3: Drawable? =
                    pckN3?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon3 != null) {
                    icon3?.setImageDrawable(pckIcon3)
                }
            } else {
                icon3?.setImageResource(R.drawable.ic_plus_circle_solid)
            }

            if (pckN4 != "No value") {
                var pckIcon4: Drawable? =
                    pckN4?.let { activity?.packageManager?.getApplicationIcon(it) }
                if (pckIcon4 != null) {
                    icon4?.setImageDrawable(pckIcon4)
                }
            } else {
                icon4?.setImageResource(R.drawable.ic_plus_circle_solid)
            }


        }
        catch (e: Exception){
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            with(sharedPref.edit()){
                remove("pckg1")
                remove("pckg2")
                remove("pckg3")
                remove("pckg4")
                apply()
            }
        }
    }

    /**
     * Saves data from shared preferences.
     * Needs to iterate over UI elements or store said elements in a list and iterate over them.
     * I'm justifying doing this manually due to the smaller number of settings required.
     */
    private fun saveData() {

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        var hasBeenChecked:Boolean? = sharedPref?.getBoolean("wasCheckedPreference", false)

        if (sharedPref != null) {
            with(sharedPref.edit()) {
                putBoolean("switch2", binding.switch2.isChecked)

                apply()
            }
            }
            if (sharedPref != null) {
                with(sharedPref.edit()) {
                    putBoolean("switchHorror", binding.switchHorror.isChecked)

                    apply()

                }
        }

        var currentCheckedState:Boolean? = sharedPref?.getBoolean("switch2", false)
        if((currentCheckedState != null) && (currentCheckedState == true)){
            if(sharedPref != null){
                with(sharedPref.edit()){
                    putBoolean("wasCheckedPreference", true)
                    apply()
                }
            }
        }

        var shutupMedal:Boolean? = sharedPref?.getBoolean("shutupPreference", false)

        if((shutupMedal != null) && (shutupMedal == false) && (hasBeenChecked != null) &&
            (hasBeenChecked == true) && (currentCheckedState != null) &&
            (currentCheckedState == false)){
            if(sharedPref != null){
                with(sharedPref.edit()){
                    putBoolean("shutupPreference", true)
                    apply()
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        saveData()
        _binding = null

    }

    /**
     * Hides UI elements and unhides the progress bar to show the user that something is happening.
     *
     */
    fun hideAndLoad(){
        binding.constraintMain.visibility = INVISIBLE
        binding.progressBar.visibility = VISIBLE

    }

    /**
     * Unhides UI elements and hides the progress bar. We're not loading anymore!
     *
     */
    fun unhideAndUnload(){
        binding.constraintMain.visibility = VISIBLE
        binding.progressBar.visibility = INVISIBLE

    }

    /** Function to open selection activity.
     *
     */
    fun onProdSelect(view:View){
        hideAndLoad()
        val intent: Intent = Intent(context, TestChoose::class.java)
        startActivity(intent)
    }

    /** Function to open selection activity.
     *
     */
    fun onUnprodSelect(view:View){
        val intent: Intent = Intent(context, ChooseUnprod1::class.java)
        startActivity(intent)
    }


    /** Function to open selection activity.
     *
     */
    fun onProd2Select(view:View){
        val intent: Intent = Intent(context, ChooseProd2::class.java)
        startActivity(intent)
    }

    /** Function to open selection activity.
     *
     */
    fun onUnprod2Select(view:View){
        val intent: Intent = Intent(context, ChooseUnprod2::class.java)
        startActivity(intent)
    }

    fun onAboutSelect(view:View){
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        var heartClick:Boolean? = sharedPref?.getBoolean("heartPreference", false)
        val intent: Intent = Intent(context, AboutActivity::class.java)
        if (sharedPref != null) {
            with(sharedPref.edit()) {
                putBoolean("heartPreference", true)

                apply()

            }
        }

        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        unhideAndUnload()
        loadData()
    }
    override fun onPause() {
        saveData()
        hideAndLoad()
        super.onPause()

    }

}