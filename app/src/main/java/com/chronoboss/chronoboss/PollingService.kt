package com.chronoboss.chronoboss

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import kotlin.properties.Delegates

/** Service class for polling the OS for usage data periodically.
 *
 */
class PollingService : Service() {

    lateinit var context:Context
    var totalBalance by Delegates.notNull<Int>()
    var timeSpentOne by Delegates.notNull<Int>()
    var timeSpentTwo by Delegates.notNull<Int>()
    var targetAppOne:String? = "first.package.name"
    var targetAppTwo:String? = "second.package.name"

    @Override
    override fun onCreate() {
        super.onCreate()
        this.context = this
    }
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStart(intent: Intent, startId: Int) {
        PollingThread().start()
    }

    /** Polling thread where the repeated code executes.
     *
     */
    internal inner class PollingThread : Thread() {

        override fun run() {

            var endTime:Long = System.currentTimeMillis()
            var startTime:Long = (endTime - 60000)
            var timeMapOne = getAppTimes(context, targetAppOne, startTime, endTime)
            timeSpentOne = timeMapOne[targetAppOne]!!
            var timeMapTwo = getAppTimes(context, targetAppTwo, startTime, endTime)
            timeSpentTwo = timeMapTwo[targetAppTwo]!!
            totalBalance -= (timeSpentTwo?.let { timeSpentOne?.plus(it) }!!)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        println("Service:onDestroy")
    }

    companion object {
        const val ACTION = "com.chronoboss.chronoboss.PollingService"
    }
}