package com.chronoboss.chronoboss

import android.app.Activity
import android.app.AppOpsManager
import android.app.usage.UsageEvents
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.provider.Settings
import java.util.ArrayList

/** Utility class providing functionality relating to queries to the OS.
 *
 */

/** Request permission to access usage stats from OS.
 */
fun requestUsageStatsPermission(activity: Activity){
    val intent = Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS)
    activity.startActivity(intent)
}

/** Check whether our app already has permission to access usage stats from OS.
 * @return Boolean based on permission status.
 */
fun hasUsageStatsPermission(context: Context, activity: Activity): Boolean {
    val applicationInfo: ApplicationInfo? =
        context?.packageName.let { activity?.packageManager?.getApplicationInfo(it, 0) }
    val appOps: AppOpsManager =
        context?.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
    val mode: Int? = applicationInfo?.let {
        appOps.checkOpNoThrow(
            AppOpsManager.OPSTR_GET_USAGE_STATS,
            it.uid,
            applicationInfo.packageName
        )
    }
    return (mode == AppOpsManager.MODE_ALLOWED)
}

/** Function to get a list of usage stats.
 * @return usageStats, a list of available usage info from the past day accessible by package name.
 *
 */

fun getUsageStats(context: Context?, beginTime: Long, endTime: Long) : List<UsageStats>{
    val usageStatsManager = context?.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
    return usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, beginTime, endTime)
}

/** Function to get the total time of the target app spent in the foreground
 * @return The total time in foreground.
 */
fun getTimeInForeground(targetPackage: UsageStats?) : Long?{
    return targetPackage?.totalTimeInForeground
}

/** Function to get the String representation of the name of the target app package
 * @return The package name (which differs from the common use app name).
 */
fun getPackageName(targetPackage: UsageStats?) : String?{
    return targetPackage?.packageName
}

/** Function to retrieve the target package
 * @return The target package.
 */
fun getTargetPackage(context: Context?, targetPackage: UsageStats?, usageStats: List<UsageStats>?) : UsageStats?{
    if (usageStats != null) {
        for(pck in usageStats){
            if(pck == targetPackage){
                return pck
            }
        }
    }
    return null
}

/** Finds the time spent on the target app passed as a parameter and returns a hashmap of app usage
 *@return appUsageMap a map of the time usage of an app.
 */
fun getAppTimes(
    context: Context,
    packageName: String?,
    beginTime: Long,
    endTime: Long
): HashMap<String, Int?> {
    var currentEvent: UsageEvents.Event
    val allEvents: MutableList<UsageEvents.Event> = ArrayList()
    val appUsageMap = HashMap<String, Int?>()
    val usageStatsManager =
        context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
    val usageEvents = usageStatsManager.queryEvents(beginTime, endTime)
    while (usageEvents.hasNextEvent()) {
        currentEvent = UsageEvents.Event()
        usageEvents.getNextEvent(currentEvent)
        if (currentEvent.packageName == packageName || packageName == null) {
            if (currentEvent.eventType == UsageEvents.Event.ACTIVITY_RESUMED
                || currentEvent.eventType == UsageEvents.Event.ACTIVITY_PAUSED
            ) {
                allEvents.add(currentEvent)
                val key = currentEvent.packageName
                if (appUsageMap[key] == null) appUsageMap[key] = 0
            }
        }
    }
    for (i in 0 until allEvents.size - 1) {
        val event0 = allEvents[i]
        val event1 = allEvents[i + 1]
        if (event0.eventType == UsageEvents.Event.ACTIVITY_RESUMED && event1.eventType == UsageEvents.Event.ACTIVITY_PAUSED && event0.className == event1.className) {
            var diff = (event1.timeStamp - event0.timeStamp).toInt()
            diff /= 1000
            var prev = appUsageMap[event0.packageName]
            if (prev == null) prev = 0
            appUsageMap[event0.packageName] = prev + diff
        }
    }

    val lastEvent = allEvents[allEvents.size - 1]
    if (lastEvent.eventType == UsageEvents.Event.ACTIVITY_RESUMED) {
        var diff = System.currentTimeMillis().toInt() - lastEvent.timeStamp.toInt()
        diff /= 1000
        var prev = appUsageMap[lastEvent.packageName]
        if (prev == null) prev = 0
        appUsageMap[lastEvent.packageName] = prev + diff
    }
    return appUsageMap
}

