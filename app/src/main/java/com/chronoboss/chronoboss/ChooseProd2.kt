package com.chronoboss.chronoboss

import android.app.Activity
import android.content.Context
import android.content.pm.ApplicationInfo
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlin.collections.ArrayList

/** Activity for choosing second productive app.
 *
 */
class ChooseProd2 : AppCompatActivity() {

    var arrayAdapter: ArrayAdapter<*>? = null
    var activity: Activity? = this
    var testDF:String? = "Testing value"
    var nullFlag = false

    override fun onBackPressed() {

    }

    /** Gets a list of apps filtering out system packages and displays their common
     * names in order for the user to be able to select one.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_choose)
        title = "Choose an app..."
        var listView: ListView = findViewById(R.id.listView)
        var bNone: Button = findViewById(R.id.b_None)
        val list = packageManager.getInstalledPackages(0)
        val listPackages: ArrayList<String> = ArrayList<String>()
        val listNames: ArrayList<String> = ArrayList<String>()
        val defaultActivityIcon = packageManager.defaultActivityIcon
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        var pckN: String? = sharedPref?.getString(getString(R.string.p1), getString(R.string.defaultvalue))
        var pckN2: String? = sharedPref?.getString(getString(R.string.p2), getString(R.string.defaultvalue))
        var pckN3: String? = sharedPref?.getString(getString(R.string.p3), getString(R.string.defaultvalue))
        var pckN4: String? = sharedPref?.getString(getString(R.string.p4), getString(R.string.defaultvalue))
        for (each in list) {
            val intentOfStartActivity = packageManager.getLaunchIntentForPackage(each.packageName)
                ?: continue
            val applicationIcon = packageManager.getActivityIcon(intentOfStartActivity)
            if (applicationIcon != null && defaultActivityIcon != applicationIcon && (each.packageName !=
                        "com.chronoboss.chronoboss") &&
                (pckN != null && each.packageName != pckN) &&
                (pckN3 != null && each.packageName != pckN3) && (pckN4 != null &&
                        each.packageName != pckN4)) {
                listNames.add(each.applicationInfo.loadLabel(packageManager).toString())
                listPackages.add(each.applicationInfo.packageName.toString())
            }
        }

        for (i in list.indices) {
            val packageInfo = list[i]
            if (packageInfo!!.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM == 0) {
                arrayAdapter = ArrayAdapter(
                    this,
                    R.layout.support_simple_spinner_dropdown_item, listNames as List<*>
                )
                listView.adapter = arrayAdapter
            }
        }

        bNone.setOnClickListener{
            val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this)
            sharedPrefs?.edit()?.remove(getString(R.string.p2))?.apply()
            nullFlag = true
            finish()
        }

        listView.setOnItemClickListener { parent, view, position, id ->
            val pckgNme = listPackages[position]
            testDF = pckgNme
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            var settingsMedal:Boolean? = sharedPref?.getBoolean("settingsPreference", false)
            if((sharedPref != null) && (!nullFlag) && (settingsMedal == false) && (testDF == "com.android.settings")){
                with(sharedPref.edit()){
                    putBoolean("settingsPreference", true).apply()
                }
            }

            if (sharedPref != null) {
                with(sharedPref.edit()) {
                    putString(getString(R.string.p2), testDF).apply()
                }
            }
            finish()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        var settingsMedal:Boolean? = sharedPref?.getBoolean("settingsPreference", false)
        if((sharedPref != null) && (!nullFlag) && (settingsMedal == false) && (testDF == "com.android.settings")){
            with(sharedPref.edit()){
                putBoolean("settingsPreference", true).apply()
            }
        }


        if (sharedPref != null && !nullFlag) {
            with(sharedPref.edit()) {
                putString(getString(R.string.p2), testDF).apply()
            }
        }

    }

    override fun onPause() {
        super.onPause()
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        var settingsMedal:Boolean? = sharedPref?.getBoolean("settingsPreference", false)
        if((sharedPref != null) && (!nullFlag) && (settingsMedal == false) && (testDF == "com.android.settings")){
            with(sharedPref.edit()){
                putBoolean("settingsPreference", true).apply()
            }
        }


        if (sharedPref != null && !nullFlag) {
            with(sharedPref.edit()) {
                putString(getString(R.string.p2), testDF).apply()
            }
        }
    }

}