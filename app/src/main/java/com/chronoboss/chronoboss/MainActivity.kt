package com.chronoboss.chronoboss

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.chronoboss.chronoboss.R.raw.*
import com.chronoboss.chronoboss.databinding.ActivityMainBinding
import com.chronoboss.chronoboss.ui.notifications.NotificationsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.NonCancellable.start
import java.util.*

/**
 * Main activity for the application, with important functions herein.
 *
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    /**
     * Quick and nasty method to generate and push notifications.
     *
     * @param title The notification's title text.
     * @param text The notification's body text.
     * @param sound The notification's sound.
     */
    fun easyNotif(title: String,text: String,sound: String, context:Context) {

        val soundUri: Uri = Uri.parse("android.resource://" + context.getPackageName().toString()+ "/"+ andrew)
        val audioAttributes: AudioAttributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
        val mediaPlayer = MediaPlayer.create(this, soundUri)

        val sound1 : MediaPlayer = MediaPlayer.create(this, R.raw.andrew_4)
        val sound2 : MediaPlayer = MediaPlayer.create(this, R.raw.andrew_3)
        val sound3 : MediaPlayer = MediaPlayer.create(this, R.raw.andrew_2)
        val sound4 : MediaPlayer = MediaPlayer.create(this, R.raw.andrew_1)
        val randomGenerator = Random()
        val randomInt = randomGenerator.nextInt(4) + 1
        when (randomInt) {
            1 -> sound1.start()
            2 -> sound2.start()
            3 -> sound3.start()
            4 -> sound4.start()
        }
        randomInt.toString()

        val i = Intent(this, MainActivity::class.java)

        val pi = TaskStackBuilder.create(this).run {
            addNextIntentWithParentStack(i)
            getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT)
        }
        var builder = NotificationCompat.Builder(this, "chronoboss")
            .setSmallIcon(R.drawable.ic_chronoprelimlogo)
            .setContentTitle(title)
            .setContentText(text)
            .setSound(soundUri)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pi)

        with(NotificationManagerCompat.from(this)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = getString(R.string.channel_name)
                val descriptionText = getString(R.string.channel_description)
                val importance = NotificationManager.IMPORTANCE_HIGH

                val channel = NotificationChannel("chronoboss", name, importance).apply {
                    description = descriptionText
                }
                channel.enableVibration(true);


                channel.setSound(soundUri, audioAttributes)


                val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                notificationManager.createNotificationChannel(channel)

            }
            notify(21, builder.build())
        }

    }

    /** Includes code to begin service.
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        startService(Intent(this , BankingService::class.java))

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)

        navView.setupWithNavController(navController)



    }

    /** Service started to ensure it runs continuously.
     *
     */
    override fun onDestroy() {

        super.onDestroy()

        startService(Intent(this , BankingService::class.java))


    }

    /** Service started to ensure it runs continuously.
     *
     */
    override fun onPause() {
        super.onPause()
        startService(Intent(this , BankingService::class.java))
    }

    /** Service started to ensure it runs continuously.
     *
     */
    override fun onResume() {
        super.onResume()
        startService(Intent(this , BankingService::class.java))
    }

    /** onClick method to open app selection activity.
     */
    fun onProdSelect(view:View){
        val intent: Intent = Intent(this, TestChoose::class.java)
        startActivity(intent)
    }

    /** onClick method to open app selection activity.
     *
     */
    fun onUnprodSelect(view:View){
        val intent: Intent = Intent(this, ChooseUnprod1::class.java)
        startActivity(intent)
    }


    /** onClick method to open app selection activity.
     *
     */
    fun onProd2Select(view:View){
        val intent: Intent = Intent(this, ChooseProd2::class.java)
        startActivity(intent)
    }

    /** onClick method to open app selection activity.
     *
     */
    fun onUnprod2Select(view:View){
        val intent: Intent = Intent(this, ChooseUnprod2::class.java)
        startActivity(intent)
    }

    fun onAboutSelect(view:View){
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        var heartClick:Boolean? = sharedPref?.getBoolean("heartPreference", false)
        val intent: Intent = Intent(this, AboutActivity::class.java)
        if (sharedPref != null) {
            with(sharedPref.edit()) {
                putBoolean("heartPreference", true)

                apply()

            }
        }

        startActivity(intent)
    }

}
