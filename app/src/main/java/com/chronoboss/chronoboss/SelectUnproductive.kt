package com.chronoboss.chronoboss

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * Class to select apps as unproductive.
 * Unused, but unsafe to delete. Remedied soon.
 */
class SelectUnproductive : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_unproductive)
    }
}