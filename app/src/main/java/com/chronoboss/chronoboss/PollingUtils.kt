package com.chronoboss.chronoboss

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock

/** Utility object to support polling service class.
 *
 */
object PollingUtils {

    /** When beginning polling service, starts a repeating alarm to continuously
     * poll OS.
     */
    fun startPollingService(context: Context, seconds: Int, cls: Class<*>?, action: String?) {
        val manager = context
            .getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, cls)
        intent.action = action
        val pendingIntent = PendingIntent.getService(
            context, 0,
            intent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        val triggerAtTime = SystemClock.elapsedRealtime()
        manager.setRepeating(
            AlarmManager.ELAPSED_REALTIME, triggerAtTime, (
                    seconds * 1000).toLong(), pendingIntent
        )
    }

    /** Clean things up when polling service stops.
     *
     */
    fun stopPollingService(context: Context, cls: Class<*>?, action: String?) {
        val manager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, cls)

        intent.action = action
        val pendingIntent = PendingIntent.getService(
            context, 0,
            intent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        manager.cancel(pendingIntent)
    }
}