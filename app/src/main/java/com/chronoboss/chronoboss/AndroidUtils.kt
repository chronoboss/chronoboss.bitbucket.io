package com.chronoboss.chronoboss

import android.os.Build

/**
 * Used for device activity and other functions.
 *
 */
object AndroidUtils {
    private var RECENT_ACTIVITY: String? = null

    /**
     * To get the Device recent screen activity
     *
     * @param className
     * @return activity
     */
    @JvmStatic
    fun isRecentActivity(className: String?): Boolean {
        return RECENT_ACTIVITY.equals(className, ignoreCase = true)
    }

    init {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            RECENT_ACTIVITY = "com.android.systemui.recents.RecentsActivity"
        } else
            RECENT_ACTIVITY = "com.android.systemui.recent.RecentsActivity"
    }
}