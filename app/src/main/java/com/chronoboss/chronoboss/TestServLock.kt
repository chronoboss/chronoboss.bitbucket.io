package com.chronoboss.chronoboss

import android.app.ActivityManager
import android.app.usage.UsageEvents
import android.app.usage.UsageStatsManager
import android.content.Context
import android.os.Build
import android.text.TextUtils
import com.chronoboss.chronoboss.AndroidUtils.isRecentActivity

/** Class providing functionality to get recent apps
 * this code has migrated to the banking service class.
 */
class TestServLock {
    /** Gets the package name of the recently used top package
     * @return topPackageName the name of the top most recently used package.
     */
    fun getRecentApps(context: Context): String {
        var topPackageName = ""
        topPackageName = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val mUsageStatsManager =
                context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
            val time = System.currentTimeMillis()
            val usageEvents = mUsageStatsManager.queryEvents(
                time - 1000 * 30,
                System.currentTimeMillis() + 10 * 1000
            )
            val event = UsageEvents.Event()
            while (usageEvents.hasNextEvent()) {
                usageEvents.getNextEvent(event)
            }
            if (event != null && !TextUtils.isEmpty(event.packageName) && event.eventType == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                return if (isRecentActivity(event.className)) {
                    event.className
                } else event.packageName
            } else {
                ""
            }
        } else {
            val am =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (isRecentActivity(componentInfo!!.className)) {
                return componentInfo.className
            }
            componentInfo.packageName
        }
        return topPackageName
    }
}