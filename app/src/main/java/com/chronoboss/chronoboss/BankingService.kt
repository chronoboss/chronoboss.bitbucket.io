package com.chronoboss.chronoboss


import android.app.*
import android.app.usage.UsageEvents
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.preference.PreferenceManager
import android.text.TextUtils
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import java.util.*

/** Class to run a service that polls the OS periodically in order to implement
 * the locking out and notification functionality.
 */
class BankingService : Service() {

    private var handler: Handler? = null

    private val runnableService: Runnable = object : Runnable {

        /**
         * Easier notification function for our purposes.
         * Throws a notification with a random andrew sound using our parameters.
         *
         * @param title Title of the thrown notification.
         * @param text Body text of the thrown  notification.
         * @param context Context of the notification.
         */
        fun easyNotif(title: String,text: String, context:Context) {

            val soundUri: Uri = Uri.parse("android.resource://" + context.getPackageName().toString()+ "/"+ R.raw.andrew)
            val audioAttributes: AudioAttributes = AudioAttributes.Builder().setUsage(
                AudioAttributes.USAGE_NOTIFICATION).build()
            val mediaPlayer = MediaPlayer.create(applicationContext, soundUri)

            val sound1 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.andrew_4)
            val sound2 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.andrew_3)
            val sound3 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.andrew_2)
            val sound4 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.andrew_1)
            val sound5 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.horror)
            val sound6 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.horror1)
            val sound7 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.horror2)
            val sound8 : MediaPlayer = MediaPlayer.create(applicationContext, R.raw.horror3)
            val sound9 :MediaPlayer = MediaPlayer.create(applicationContext, R.raw.horror4)
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val randomGenerator = Random()

            if (sharedPref.getBoolean("switch2",true)){
                when (randomGenerator.nextInt(4) + 1) {
                    1 -> sound1.start()
                    2 -> sound2.start()
                    3 -> sound3.start()
                    4 -> sound4.start()
                }
            } else {
                sharedPref.getBoolean("switch2",false)
            }
            if (sharedPref.getBoolean("switchHorror",true)) {
                when (randomGenerator.nextInt(5) + 1) {
                    1 -> sound5.start()
                    2 -> sound6.start()
                    3 -> sound7.start()
                    4 -> sound8.start()
                    5 -> sound9.start()

                }
            } else {
                sharedPref.getBoolean("switchHorror",false)
            }

            val i = Intent(applicationContext, MainActivity::class.java)

            val pi = TaskStackBuilder.create(applicationContext).run {
                addNextIntentWithParentStack(i)
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            var builder = NotificationCompat.Builder(applicationContext, "chronoboss")
                .setSmallIcon(R.drawable.ic_chronoprelimlogo)
                .setContentTitle(title)
                .setContentText(text)
                .setSound(soundUri)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pi)

            with(NotificationManagerCompat.from(applicationContext)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val name = getString(R.string.channel_name)
                    val descriptionText = getString(R.string.channel_description)
                    val importance = NotificationManager.IMPORTANCE_HIGH

                    val channel = NotificationChannel("chronoboss", name, importance).apply {
                        description = descriptionText
                    }
                    channel.enableVibration(true);


                    channel.setSound(soundUri, audioAttributes)


                    val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                    notificationManager.createNotificationChannel(channel)

                }
                notify(21, builder.build())
            }

        }


        /** Code that executes periodically to poll OS.
         *
         */
        override fun run() {
            val err: String = "No value"
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            var pckN: String? = sharedPref?.getString("pckg1", err)
            var pckN2: String? = sharedPref?.getString("pckg2", err)
            var pckN3: String? = sharedPref?.getString("pckg3", err)
            var pckN4: String? = sharedPref?.getString("pckg4", err)
            var startTime: Long = (System.currentTimeMillis() - 86400000)
            var endTime: Long = System.currentTimeMillis()

            var pck1 = getPckUsg(applicationContext, pckN, startTime, endTime)
            var Long1 =
                pck1?.totalTimeInForeground //Toast.makeText(context, Long1.toString(), Toast.LENGTH_LONG).show()
            var int1:Int? = (Long1?.div(60000))?.toInt()
            var pck2 = getPckUsg(applicationContext, pckN2, startTime, endTime)
            var Long2 = pck2?.totalTimeInForeground
            var int2:Int? = (Long2?.div(60000))?.toInt()
            var pck3 = getPckUsg(applicationContext, pckN3, startTime, endTime)
            var Long3 = pck3?.totalTimeInForeground
            var int3:Int? = (Long3?.div(60000))?.toInt()
            var pck4 = getPckUsg(applicationContext, pckN4, startTime, endTime)
            var Long4 = pck4?.totalTimeInForeground
            var int4:Int? = (Long4?.div(60000))?.toInt()
            var totalBalanceLong: Long = 0
            if (Long1 != null) {
                totalBalanceLong += Long1
            }
            if(Long2 != null){
                totalBalanceLong += Long2
            }

            if(Long3 != null){
                totalBalanceLong -= Long3
            }


            if(Long4 != null){
                totalBalanceLong -= Long4
            }


            var str2:String? = getForegroundApp(applicationContext)

            if((totalBalanceLong <= 0) && ((pckN3 == str2) || (pckN4 == str2))){
                easyNotif("Chronoboss", "You don't have enough minutes to do this.",
                applicationContext)
                var nextIntent = Intent(applicationContext, MainActivity::class.java)
                nextIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                nextIntent.action = Intent.ACTION_MAIN
                startActivity(nextIntent)
            }
            handler!!.postDelayed(this, DEFAULT_SYNC_INTERVAL)
        }
    }

    /** Get the app currently running in the foreground
     * @return foregroundApp the name of the package currently in foreground.
     */
    fun getForegroundApp(context: Context): String? {
        var foregroundApp: String? = null
        val mUsageStatsManager =
            context.getSystemService(Service.USAGE_STATS_SERVICE) as UsageStatsManager
        val time = System.currentTimeMillis()
        val usageEvents = mUsageStatsManager.queryEvents(time - 1000 * 3600, time)
        val event = UsageEvents.Event()
        while (usageEvents.hasNextEvent()) {
            usageEvents.getNextEvent(event)
            if (event.eventType == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                foregroundApp = event.packageName
            }
        }
        return foregroundApp
    }


    /** Code that executes when the service starts
     * @return START_STICKY which tells it to continue running even upon exit.
     */
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        handler = Handler()

        handler!!.post(runnableService)

        return START_STICKY

    }


    override fun onBind(intent: Intent): IBinder? {

        return null

    }

    /** Code to get a target package from usage stats
     * @return usages[str] the UsageStats object of the target package.
     */
    fun getPckUsg(context: Context?, str: String?, beginTime: Long, endTime: Long): UsageStats? {
        val usageStatsManager =
            context?.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
        var usages: Map<String, UsageStats> =
            usageStatsManager.queryAndAggregateUsageStats(beginTime, endTime)
        return usages[str]
    }


    override fun onDestroy() {

        handler!!.removeCallbacks(runnableService)

        super.onDestroy()
        startService(Intent(this , BankingService::class.java))

    }

    /** Function to get a list of usage stats.
     * @return list of usage stats.
     */
    fun getUsageStatsList(context: Context): List<UsageStats?>? {
        val usm = context?.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
        val calendar = Calendar.getInstance()
        val endTime = calendar.timeInMillis
        calendar.add(Calendar.DATE, -1)
        val startTime = calendar.timeInMillis
        return usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, startTime, endTime)
    }

    /** Defines the polling interval of 10s.
     *
     */
        companion object {

        const val DEFAULT_SYNC_INTERVAL = (10 * 1000).toLong()

    }

    /** Gets a list of recently used apps.
     * @return topPackageName the name of the top most recently used app.
     */
    fun getRecentApps(context: Context): String {
        var topPackageName = ""
        topPackageName = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val mUsageStatsManager =
                context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
            val time = System.currentTimeMillis()
            val usageEvents = mUsageStatsManager.queryEvents(
                time - 1000 * 30,
                System.currentTimeMillis() + 10 * 1000
            )
            val event = UsageEvents.Event()
            while (usageEvents.hasNextEvent()) {
                usageEvents.getNextEvent(event)
            }
            if (event != null && !TextUtils.isEmpty(event.packageName) && event.eventType == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                return if (AndroidUtils.isRecentActivity(event.className)) {
                    event.className
                } else event.packageName
            } else {
                ""
            }
        } else {
            val am =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (AndroidUtils.isRecentActivity(componentInfo!!.className)) {
                return componentInfo.className
            }
            componentInfo.packageName
        }
        return topPackageName
    }

}