package com.chronoboss.chronoboss

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.SharedPreferences
import kotlinx.android.synthetic.main.fragment_notifications.*

/**
 * Class to select apps as productive.
 * Unused, but unsafe to delete. Remedied soon.
 */
class SelectProductive : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_productive)
    }
}