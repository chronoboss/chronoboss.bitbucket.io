package com.chronoboss.chronoboss.ui.notifications

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.chronoboss.chronoboss.MainActivity
import com.chronoboss.chronoboss.R
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

/**
 * UI test for the settings menu fragment.
 * Cycles various menu elements to check they're working.
 * Incomplete but has been helpful.
 */
class NotificationsFragmentTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)

    @Test
            /**
             * Cycles view.
             *
             */
    fun view_the_cycle(){
     Espresso.onView(withId(R.id.container)).perform(click()).check(matches(isDisplayed()))
       Espresso.onView(withId(R.id.navigation_home)).perform(click()).check(matches(isDisplayed()))
        Espresso.onView(withId(R.id.navigation_dashboard)).perform(click()).check(matches(isDisplayed()))

    }

    @Test
            /**
             * Checks switches operational.
             *
             */
    fun turn_nice_notification(){
        Espresso.onView(withId(R.id.navigation_notifications)).perform(click()).check(matches(isDisplayed()))

    }

   @Test
           /**
            * Selects productive apps.
            *
            */
    fun select_productive_apps(){
        Espresso.onView(withId(R.id.navigation_notifications)).perform(click()).check(matches(isDisplayed()))
        Espresso.onView(withId(R.id.addProdApp)).perform(click())
    }

    @Test
            /**
             * Selects unproductive apps.
             *
             */
    fun select_unproductive_apps(){
      Espresso.onView(withId(R.id.navigation_notifications)).perform(click()).check(matches(isDisplayed()))

  }

}